#include <iostream>
#include "Display/display.h"
#include "Shaders/shader.h"
#include "Mesh/Mesh.h"
#include "Textures/texture.h"
#include "Transform/transform.h"
int main(){
	Display d(800,600,"Hello World");	
	
	Vertex vertices[] = {
		Vertex(glm::vec3(-0.5,-0.5,0),glm::vec2(0,0)),
		Vertex(glm::vec3(0,0.5,0),glm::vec2(0.5,1)),
		Vertex(glm::vec3(0.5,-0.5,0),glm::vec2(1,0))
	};
	Mesh mesh(vertices,sizeof(vertices)/sizeof(vertices[0]));
	Shader s("Shaders/basic");
	Texture texture("Textures/bricks.jpg");
	Transform t;
	float counter=0;
	while(!d.isClosed()){
		d.Clear(0.0f,0.15f,0.3f,1.0f);
		s.Bind();
		float cosc = cosf(counter);
		t.SetPos(glm::vec3(sinf(counter),0,0));
		t.SetRot(glm::vec3(0,0,counter));
		t.SetScale(glm::vec3(cosc,cosc,cosc));
		texture.Bind(0);
		s.Update(t);
		mesh.Draw();
		d.Update();
		counter+=0.01f;
	}
	return 0;
}
