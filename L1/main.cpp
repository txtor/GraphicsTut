#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
int main(void)
{
	GLFWwindow* win;
	if(!glfwInit())
		return -1;
	

	win = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
	if(!win)
	{
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(win);
	if(glewInit()!=GLEW_OK)
		std::cout<<"Error"<<std::endl;
	std::cout<<glGetString(GL_VERSION)<<std::endl;
	while(!glfwWindowShouldClose(win)){
		glClear(GL_COLOR_BUFFER_BIT);
		glBegin(GL_TRIANGLES);
		glVertex2f(-0.5f,-0.5f);
		glVertex2f(0.0f,0.5f);
		glVertex2f(0.5f,-0.5f);
		glEnd();

		glfwSwapBuffers(win);
		glfwPollEvents();
	}
	glfwTerminate();
}
