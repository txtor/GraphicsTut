#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <fstream>
#include <sstream>

struct ShaderProgramSource{
	std::string vertexSource;
	std::string fragmentSource;
};
static void GLClearError(){
	while(!glGetError());
}

static bool GLLogCall(){
	while(GLenum err = glGetError()){
		std::cout<<"[OpenGL Error] ("<< err<<")"<<std::endl;
		return false;
	}
	return true;
}
static ShaderProgramSource ParseShader(const std::string& file){
	std::ifstream infile(file);
	enum class ShaderType
	{
		NONE=-1,VERTEX=0,FRAGMENT=1

	};
	std::string line;
	std::stringstream ss[2];
	ShaderType type=ShaderType::NONE;
	while(getline(infile,line))
	{
		if(line.find("#shader") != std::string::npos)
		{
			if(line.find("vertex") != std::string::npos)
				type=ShaderType::VERTEX;
			else if(line.find("fragment") != std::string::npos)
				type=ShaderType::FRAGMENT;
		}
		else{
			ss[(int)type] << line<<'\n';
		}
	}
	return { ss[0].str(),ss[1].str() };
}
static unsigned int CompileShader(unsigned int type,const std::string& source)
{
	unsigned int id = glCreateShader(type);
	const char* src=source.c_str();
	glShaderSource(id,1,&src,nullptr);
	glCompileShader(id);
	int res;
	glGetShaderiv(id,GL_COMPILE_STATUS,&res);
	if(res == GL_FALSE){
		int length;
		glGetShaderiv(id,GL_INFO_LOG_LENGTH,&length);
		char*  message = (char*)alloca(length*sizeof(char));
		glGetShaderInfoLog(id,length,&length,message);
		std::cout<< "Failed To Compile "<<
			(type==GL_VERTEX_SHADER?"vertex":"fragment")<<" Shader"<<std::endl;
		std::cout<<message<<std::endl;
		glDeleteShader(id);
		return 0;
	}
		
	return id;	
}
static unsigned int CreateShader(const std::string& vertexShader,const std::string& fragmentShader){
	unsigned int program = glCreateProgram();
	unsigned int vs = CompileShader(GL_VERTEX_SHADER,vertexShader);
	unsigned int fs = CompileShader(GL_FRAGMENT_SHADER,fragmentShader);
	glAttachShader(program,vs);
	glAttachShader(program,fs);
	glLinkProgram(program);
	glValidateProgram(program);
	glDeleteShader(vs);
	glDeleteShader(fs);

	return program;

}
int main(void)
{
	GLFWwindow* win;
	if(!glfwInit())
		return -1;
	

	win = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
	if(!win)
	{
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(win);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	if(glewInit()!=GLEW_OK)
		std::cout<<"Error"<<std::endl;
	std::cout<<glGetString(GL_VERSION)<<std::endl;
	float position[]={
		-0.5f,-0.5f, // 0 
		0.5f,-0.5f,  // 1
		0.5f,0.5f,   // 2
		-0.5f,0.5f   // 3
	};
	unsigned int indices[]={
		0,1,2,
		2,3,0
	};
	unsigned int vao;
	glGenVertexArrays(1,&vao);
	glBindVertexArray(vao);
	unsigned int buffer;
	glGenBuffers(1,&buffer);
	glBindBuffer(GL_ARRAY_BUFFER,buffer);
	glBufferData(GL_ARRAY_BUFFER,6*2*sizeof(float),position,GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,sizeof(float)*2,0);

	unsigned int ibo;
	glGenBuffers(1,&ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(unsigned int),indices,GL_STATIC_DRAW);
	ShaderProgramSource src = ParseShader("shdr/basic.shader");	
	unsigned int shader = CreateShader(src.vertexSource, src.fragmentSource);
	glUseProgram(shader);
	int location = glGetUniformLocation(shader, "u_Color");
	float r =0.0f;
	float inc = 0.05f;
	/* Draw Loop*/
	while(!glfwWindowShouldClose(win)){
		glClear(GL_COLOR_BUFFER_BIT);
		glUniform4f(location,r,0.2,0.8,1.0);
		glBindBuffer(GL_ARRAY_BUFFER,buffer);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,sizeof(float) * 2,0);

		glDrawElements(GL_TRIANGLES,6,GL_UNSIGNED_INT,nullptr);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,ibo);
		GLLogCall();
		if(r>1.0) inc =-0.05f;
		else if(r<0.0) inc=0.05;
	       	r+=inc;
		glfwSwapBuffers(win);
		glfwPollEvents();
	}
	glDeleteShader(shader);
	glfwTerminate();
}
