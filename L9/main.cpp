#include <iostream>
#include "Display/display.h"
#include "Shaders/shader.h"
#include "Mesh/Mesh.h"
#include "Textures/texture.h"
int main(){
	Display d(800,600,"Hello World");	
	
	Vertex vertices[] = {
		Vertex(glm::vec3(-0.5,-0.5,0),glm::vec2(0,0)),
		Vertex(glm::vec3(0,0.5,0),glm::vec2(0.5,1)),
		Vertex(glm::vec3(0.5,-0.5,0),glm::vec2(1,0))
	};
	Mesh mesh(vertices,sizeof(vertices)/sizeof(vertices[0]));
	Shader s("Shaders/basic");
	Texture texture("Textures/bricks.jpg");
	while(!d.isClosed()){
		d.Clear(0.0f,0.15f,0.3f,1.0f);
		s.Bind();
		texture.Bind(0);
		mesh.Draw();
		d.Update();
	}
	return 0;
}
