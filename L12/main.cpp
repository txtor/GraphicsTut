#include <iostream>
#include <display.h>
#include <shader.h>
#include <Mesh.h>
#include <texture.h>
#include <transform.h>
#include <camera.h>

#define WIDTH 800
#define HEIGHT 600
int main(int argc,char* argv[]){
	Display d(WIDTH,HEIGHT,"Hello World");	
	
	/*Vertex vertices[] = {
		Vertex(glm::vec3(-0.5,-0.5,0),glm::vec2(0,0)),
		Vertex(glm::vec3(0,0.5,0),glm::vec2(0.5,1)),
		Vertex(glm::vec3(0.5,-0.5,0),glm::vec2(1,0))
	};

	unsigned int indices[] = {0,1,2};*/
	//Mesh mesh(vertices,sizeof(vertices)/sizeof(vertices[0]),indices,sizeof(indices)/sizeof(indices[0]));
	if(argc<2){
		std::cout<<"No File Specified!"<<std::endl;
	       	return 0;
	}
	Mesh mesh2(argv[1]);
	Shader s("Shaders/basic");
	Texture texture("Textures/bricks.jpg");
	Transform t;
	Camera c(glm::vec3(0,0,-4),70.0f,(float)WIDTH/HEIGHT,0.01f,1000.0f);
	float counter=0;
	while(!d.isClosed()){
		d.Clear(0.0f,0.15f,0.4f,1.0f);
		s.Bind();
		float cosc = cosf(counter);
		t.SetPos(glm::vec3(sinf(counter),0,cosc));
		//t.SetPos(glm::vec3(0,0,cosc));
		t.SetRot(glm::vec3(counter,counter,counter));
		//t.SetScale(glm::vec3(cosc,cosc,cosc));
		texture.Bind(0);
		s.Update(t,c);
		mesh2.Draw();
		d.Update();
		counter+=0.01f;
	}
	return 0;
}
